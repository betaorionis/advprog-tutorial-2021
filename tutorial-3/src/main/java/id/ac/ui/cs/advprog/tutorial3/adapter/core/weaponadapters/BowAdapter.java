package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

public class BowAdapter implements Weapon {

    private Bow bow;
    private Boolean aimShotMode;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.aimShotMode = false;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(aimShotMode);
    }

    @Override
    public String chargedAttack() {
        this.aimShotMode = !this.aimShotMode;

        return (this.aimShotMode ? "Entering" : "Leaving") + " Aim Shot mode";
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        return this.bow.getHolderName();
    }
}
