package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;

import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;

    @Override
    public List<Weapon> findAll() {
        List<Weapon> allWeapon = weaponRepository.findAll();

        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null)
                allWeapon.add(new BowAdapter(bow));
        }

        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null)
                allWeapon.add(new SpellbookAdapter(spellbook));
        }

        return allWeapon;
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon;
        weapon = weaponRepository.findByAlias(weaponName);

        if (weapon == null) {
            Bow currBow = bowRepository.findByAlias(weaponName);
            if (currBow != null)
                weapon = new BowAdapter(currBow);
        }

        if (weapon == null) {
            Spellbook currSpellbook = spellbookRepository.findByAlias(weaponName);
            if (currSpellbook != null)
                weapon = new SpellbookAdapter(currSpellbook);
        }

        String attackTypeStr = attackType == 0 ? "(normal attack)" : "(charged attack)";
        String attackTypeWeap = attackType == 0 ? weapon.normalAttack() : weapon.chargedAttack();

        logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " with " + attackTypeStr
                + ": " + attackTypeWeap);

        weaponRepository.save(weapon);

    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
