package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {

    public CaesarTransformation(){
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){

        String text = spell.getText();
        Codex codex = spell.getCodex();
        int shift = encode ? 1 : -1;
        int codexSize = codex.getCharSize();
        int n = text.length();
        char[] res = new char[n];

        for(int i = 0; i < text.length(); i++){
            char oldChar = text.charAt(i);
            int charIndex = codex.getIndex(oldChar);
            int newCharIndex = charIndex + shift;
            newCharIndex = newCharIndex < 0 ? codexSize + newCharIndex : newCharIndex % codexSize;
            res[i] = codex.getChar(newCharIndex);
        }

        
        System.out.println(encode);
        System.out.println(text);
        System.out.println(res);
        return new Spell(new String(res), codex);
    }
}
