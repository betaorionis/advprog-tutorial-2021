package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenFactoryTest {
    // Ingridients:
    // Noodle: Ramen
    // Meat: Pork
    // Topping: Boiled Egg
    // Flavor: Spicy

    private static InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeAll
    public static void setUp() {
        inuzumaRamenFactory = new InuzumaRamenFactory();
    }

    @Test
    public void testInuzumaRamenFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(InuzumaRamenFactory.class.getModifiers()));
    }

    @Test
    public void testInuzumaRamenFactoryImplementsIngridientsFactory() {
        assertTrue(IngridientsFactory.class.isAssignableFrom(InuzumaRamenFactory.class));
    }

    @Test
    public void testInuzumaRamenFactoryReturnsCorrectIngridients() {
        assertTrue(inuzumaRamenFactory.getNoodle() instanceof Ramen);
        assertTrue(inuzumaRamenFactory.getMeat() instanceof Pork);
        assertTrue(inuzumaRamenFactory.getTopping() instanceof BoiledEgg);
        assertTrue(inuzumaRamenFactory.getFlavor() instanceof Spicy);
    }

}
