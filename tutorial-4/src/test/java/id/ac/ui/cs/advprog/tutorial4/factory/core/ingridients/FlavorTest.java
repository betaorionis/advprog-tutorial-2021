package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class FlavorTest {

    private static List<Flavor> allFlavors;

    @BeforeAll
    public static void setUp(){
        allFlavors = new ArrayList<>();
        allFlavors.add(new Salty());
        allFlavors.add(new Spicy());
        allFlavors.add(new Sweet());
        allFlavors.add(new Umami());
    }

    @Test
    public void testAllFlavorsAreConcreteClass() {
        for (Flavor concreteFlavor : allFlavors) {
            Class<?> concreteFlavorClass = concreteFlavor.getClass();

            assertFalse(Modifier.isAbstract(concreteFlavorClass.getModifiers()));
        }
    }

    @Test
    public void testAllFlavorsImplementsFlavor() {
        for (Flavor concreteFlavor : allFlavors) {
            Class<?> concreteFlavorClass = concreteFlavor.getClass();

            assertTrue(Flavor.class.isAssignableFrom(concreteFlavorClass));
        }
    }

    @Test
    public void testAllFlavorsHaveDifferentDescription() {
        HashSet<String> allDescriptions = new HashSet<>();

        for (Flavor concreteFlavor : allFlavors) {
            String description = concreteFlavor.getDescription();

            assertFalse(allDescriptions.contains(description));
            allDescriptions.add(description);
        }
    }

}
