package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class InuzumaRamenTest {
    // Ingridients:
    // Noodle: Ramen
    // Meat: Pork
    // Topping: Boiled Egg
    // Flavor: Spicy

    private static String ramenName;
    private static InuzumaRamen inuzumaRamen;

    @BeforeAll
    public static void setUp() {
        ramenName = "ramennn";
        inuzumaRamen = new InuzumaRamen(ramenName);
    }

    @Test
    public void testCorrectIngredients() {
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }

    @Test
    public void testCorrectName() {
        assertEquals(ramenName, inuzumaRamen.getName());
    }
}
