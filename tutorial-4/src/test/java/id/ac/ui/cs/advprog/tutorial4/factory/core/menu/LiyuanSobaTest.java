package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class LiyuanSobaTest {
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet

    private static String sobaName;
    private static LiyuanSoba liyuanSoba;

    @BeforeAll
    public static void setUp() {
        sobaName = "sobaaa";
        liyuanSoba = new LiyuanSoba(sobaName);
    }

    @Test
    public void testCorrectIngredients() {
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }

    @Test
    public void testCorrectName() {
        assertEquals(sobaName, liyuanSoba.getName());
    }
}
