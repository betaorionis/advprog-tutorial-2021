package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;

public class MenuRepositoryTest {
    @Test
    public void testAddMenu() {
        MenuRepository menuRepository = new MenuRepository();
        menuRepository.add(new InuzumaRamen("ramennn"));
        menuRepository.add(new LiyuanSoba("sobaaa"));
        menuRepository.add(new MondoUdon("udonnn"));
        menuRepository.add(new SnevnezhaShirataki("shiratakiii"));

        assertEquals(4, menuRepository.getMenus().size());
    }
}
