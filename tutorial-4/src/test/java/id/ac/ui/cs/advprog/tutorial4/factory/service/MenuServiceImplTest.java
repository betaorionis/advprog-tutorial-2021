package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class MenuServiceImplTest {

    private static MenuServiceImpl menuService;
    private MenuRepository repo;

    @BeforeAll
    static void setUp() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceCreateMenu() throws Exception {
        String sobaName = "sobaaa";
        menuService.createMenu(sobaName, "Soba");
        String udonName = "udonnn";
        menuService.createMenu(udonName, "Udon");
        String ramenName = "ramennn";
        menuService.createMenu(ramenName, "Ramen");
        String shiratakiName = "shiratakii";
        menuService.createMenu(shiratakiName, "Shirataki");

        System.out.println(menuService.getMenus().size());
        assertEquals(8, menuService.getMenus().size());
    }
}
